# Forms Down

Forms Down is a Markdown like syntax for generating HTML forms.

This is currently a **WORK IN PROGRESS**

## Goal

The goal of this project is to devlop a forms markup similar to Markdown for generating HTML forms. 

## Syntax

Basic syntax 
```
?{type}("label" "value" "placeholder" "id"){.class}
```

- type: the type of the <input> element (required)
- label: the label
- value: the value of the <input> element (optional)
- placeholder: the placeholder for the <input> element (optional)
- id: ID of the form.
- class: the class of the <input> element (optional)

## Forms Down Quick (fdq)

Forms Down Quick is an alternative syntax for generating forms.

```
?{fdq}label|value|placeholder|id|.class
```

## Input Types

Forms Down is comptable with the following form types:

### Basic
- text input
- textarea input
- password
- checkbox
- radio
- file
- image
- submit
- button

## Extended
- email
- url
- number
- range
- date (month/week/time)
- color
- tel

