
## Login

### Forms Down - Login Form
This is a basic login form in Forms Down.

```
?{text}("Username" "" "Username:" "username")
?{password}("Password" "" "Password" "pwd")
?{submit}("" "Submit")
```

Which would render the following HTML:

```
<form>
  <label for="username">Username:</label><br>
  <input type="text" id="username" name="username"><br>
  <label for="pwd">Password:</label><br>
  <input type="password" id="pwd" name="pwd">
  <input type="submit" value="Submit">
</form>
```

## Forms Down - Signup

This is a basic signup form in Forms Down.

```
?{text}("First Name" "" "Dave" "fname")
?{text}("Last Name" "" "Anderspoon" "lname")
?{email}("email" "" "email@domain.com" "email")
?{password}("Password" "" "Password" "pwd")
?{submit}("" "Submit")
```

Which would render the following HTML:

```
<form>
  <label for="fname">First name:</label><br>
  <input type="text" id="fname" name="fname"><br>
  <label for="lname">Last name:</label><br>
  <input type="text" id="lname" name="lname">
  <input type="email" id="email" name="email">
  <input type="password" id="pwd" name="pwd">
  <input type="submit" value="Submit">
</form>
```

## Forms Down - Quick - Signup

Forms Down Quick is an alternative syntax that uses | as a delimeter value.

```
?{fdq}(text|First Name||Dave|fname)
?{fdq}(text|Last Name||Dave|lname)
?{fdq}(email|email|||dave@anderspoon.net|email)
?{fdq}(password||password|pwd)
?{submit}("" "Submit")

```

Which would render the following HTML:

```
<form>
  <label for="fname">First name:</label><br>
  <input type="text" id="fname" name="fname"><br>
  <label for="lname">Last name:</label><br>
  <input type="text" id="lname" name="lname">
  <input type="email" id="email" name="email">
  <input type="password" id="pwd" name="pwd">
  <input type="submit" value="Submit">
</form>
```

## Example
For example, to create a Twitter Bootstrap form:
```
?{text}("Name" "" "Name..."){.form-control}
?{email}("Email" "" "Email..."){.form-control}
?{text}("Subject" "" "Subject..."){.form-control}
?{textarea}("Message" "" "Message..." 3*20){.form-control}
?{submit}("" "Send!"){.form-control}
```
